//
//  AppDelegate.h
//  DemoGit
//
//  Created by ios15 on 8/22/13.
//  Copyright (c) 2013 Hans-Eric Gronlund. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

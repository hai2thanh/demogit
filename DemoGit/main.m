//
//  main.m
//  DemoGit
//
//  Created by ios15 on 8/22/13.
//  Copyright (c) 2013 Hans-Eric Gronlund. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
